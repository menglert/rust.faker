use chrono::{NaiveDate, NaiveDateTime};
use rand::Rng;
use rand::prelude::SliceRandom;
use std::fs::File;
use std::io::Write;
use uuid::Uuid;

fn main() {
    let events = vec!["click", "impression", "pageview", "wish", "wishlist"];
    let domains = vec!["amazon.com", "ebay.com", "walmart.com", "apple.com", "etsy.com", "target.com", "homedepot.com", "bestbuy.com",
    "macys.com", "wayfair.com", "chewy.com", "nike.com", "kohls.com", "aliexpress.com", "costco.com", "lowes.com",
    "overstock.com", "wish.com", "sephora.com", "nordstrom.com", "ikea.com", "ulta.com", "zara.com", "hm.com",
    "gap.com", "qvc.com", "zappos.com", "lululemon.com", "newegg.com", "walgreens.com", "bathandbodyworks.com",
    "anthropologie.com", "samsclub.com", "asos.com", "victoriassecret.com", "urbanoutfitters.com", "bhphotovideo.com",
    "rei.com", "staples.com", "cvs.com", "officedepot.com", "dickssportinggoods.com", "bedbathandbeyond.com",
    "jcpenney.com", "michaels.com", "forever21.com", "petco.com", "cabelas.com", "gamestop.com", "hottopic.com",
    "barnesandnoble.com", "ae.com", "potterybarn.com", "crateandbarrel.com", "sears.com", "dsw.com", "zappos.com",
    "fanatics.com", "llbean.com", "footlocker.com", "lego.com", "autozone.com", "adidas.com", "underarmour.com",
    "saksoff5th.com", "coach.com", "ralphlauren.com", "bloomingdales.com", "finishline.com", "freepeople.com",
    "reebok.com", "oldnavy.com", "jcrew.com", "patagonia.com", "thenorthface.com", "vans.com", "journeys.com",
    "tillys.com", "zumiez.com", "aeropostale.com", "hollisterco.com", "abercrombie.com", "americaneagle.com",
    "express.com", "guess.com", "topshop.com", "aritzia.com", "nastygal.com", "missguidedus.com", "forever21.com",
    "hm.com", "zara.com", "uniqlo.com", "mango.com", "bershka.com", "pullandbear.com", "stradivarius.com",
    "massimodutti.com", "zarahome.com", "stories.com", "cosstores.com", "theoutnet.com", "yoox.com", "farfetch.com"];

    let mut rng = rand::thread_rng();

    //Change any randomization settings here. e.g. Total Row Count --> unique_ids
    let sessions: Vec<Uuid> = (0..75_000_000).map(|_| Uuid::new_v4()).collect();
    let users: Vec<Uuid> = (0..7_500_000).map(|_| Uuid::new_v4()).collect();
    let unique_ids: Vec<Uuid> = (0..2_000_000_000).map(|_| Uuid::new_v4()).collect();

    let mut writer = File::create("fake_2B.csv").expect("Unable to create CSV file");
    writeln!(writer, "unique_id,user_id,tracking_id,session_id,event,domain,timestamp").expect("Unable to write CSV header");


    for unique_id in unique_ids {
        let user_id = users.choose(&mut rng).unwrap();
        let tracking_id = Uuid::new_v4();
        let session_id = sessions.choose(&mut rng).unwrap();
        let event = events.choose(&mut rng).unwrap();
        let domain = domains.choose(&mut rng).unwrap();
        let year = rng.gen_range(2021..=2023);
        let month = rng.gen_range(1..=12);
        let day = rng.gen_range(1..=28); 
        let hour = rng.gen_range(0..=23);
        let minute = rng.gen_range(0..=59);
        let second = rng.gen_range(0..=59);

        let faked_date = NaiveDate::from_ymd_opt(year, month, day).unwrap();
        let faked_time = chrono::NaiveTime::from_hms_opt(hour, minute, second).unwrap();    
        let timestamp = NaiveDateTime::new(faked_date, faked_time);

        writeln!(writer, "{},{},{},{},{},{},{}", unique_id, user_id, tracking_id, session_id, event, domain, timestamp)
        .expect("Unable to write CSV record");
    }
}