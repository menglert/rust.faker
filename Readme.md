# Rust Fake Data Generator
**DISCLAIMER: This is a very simple and not optimized Rust program to generate a fake dataset**
## Pre-Requisites
* Install `gcc`, `cmake`
* Install Rust
  * `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
## Build
* `cargo build --release`
## Run
* `./target/release/faker`
## Adjust
### Change the Output file
* Change the following line to change the output filename or the target folder
```
33    let mut writer = File::create("fake_2B.csv").expect("Unable to create CSV file");

```
### Change the size of the target dataset
* Change the following lines to adjust the size of the output
```
29    let sessions: Vec<Uuid> = (0..75_000_000).map(|_| Uuid::new_v4()).collect();
30    let users: Vec<Uuid> = (0..7_500_000).map(|_| Uuid::new_v4()).collect();
31    let unique_ids: Vec<Uuid> = (0..2_000_000_000).map(|_| Uuid::new_v4()).collect();
```
